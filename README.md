# Graphql Apollo Federation Example

A small example of using Apollo Federation technology using Mongodb, NodeJs and Expressjs

![](img/tittle.jpg)

# What is it?
[Apollo Web Page](https://www.apollographql.com/docs/apollo-server/federation/introduction/)

# How it Works?
![](img/federation_schema.png)
Apollo Federation introduces an important new principle to modular schema design: 
proper separation by concern. It's the key architectural quality that allows separate 
teams to collaborate on a single product-centric graph without stepping on each other's toes.
```
    type User @key(fields: "id") {  |   type Comment @key(fields: "id") {
      id: ID!                       |       id: ID!
      username: String,             |       body: String
      email: String,                |       author: User @provides(fields: "username,email")
      bio: String,                  |   }
      hash: String,                 |   extend type User @key(fields: "id") {
      salt: String,                 |       id: ID! @external
      idsocial: String,             |       username: String @external
    }                               |       email: String @external
                                    |}
```
Apollo Federation allows you to extend an existing type with additional fields, using GraphQL's 
extend type functionality. That means we can break up a schema across boundaries that 
correspond to features or team structure.

# The example
The structure of the example:
```
    src
        index.js (The Apollo Gateway)
            models (Mongo Models)
                Users.js
                Coments.js
            users (User Graphql Service)
                index.js
                resolvers.js
                typeDefs,js
             coments (Coments Graphql Service)
                index.js
                resolvers.js
                typeDefs,js
```
## Apollo Gateway
The apollo gateway joins all the graphql services we want tu use to brign us all 
the services unificated and expose only a single data graph.

### Apollo Gateway Code Example
```
const startServer = async () => {
  const app = express();

  const gateway = new ApolloGateway({
    serviceList: [
      { name: 'Users', url: 'http://localhost:4000/graphql' },
      { name: 'Coments', url: 'http://localhost:4002/graphql' }
      // more services
    ],
  });
  
  // Pass the ApolloGateway to the ApolloServer constructor
  const server = new ApolloServer({
    gateway,
  
    // Disable subscriptions (not currently supported with ApolloGateway)
    subscriptions: false,
  });

  server.applyMiddleware({ app });

  app.listen({ port: 3999 }, () =>
    console.log(`🚀 Server ready at http://localhost:3999${server.graphqlPath}`)
    );
}
```

## Graphql Federated Schemas
The schemas in the repository illustrate several important conventions of Apollo Federation:

    An implementing service can reference a type that's defined by another implementing service. 
    For example, the Coment type includes an Author field of type User, even though the User type 
    is defined in a different service.An implementing service must add the @key directive to a 
    type's definition in order for other services to be able to reference or extend that type. 
    This directive tells other services which fields to use to uniquely identify a particular 
    instance of the type.
    
##  Example
```
    type Comment @key(fields: "id") {
      id: ID!
      body: String
      author: User @provides(fields: "username,email")
    }
    
    extend type User @key(fields: "id") {
      id: ID! @external
      username: String @external
      email: String @external
    }
```
