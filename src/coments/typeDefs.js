import { gql } from "apollo-server-express";

export const typeDefs = gql`
extend type Query {
  all_comments: [Comment]
}
type Comment @key(fields: "id") {
  id: ID!
  body: String
  author: User @provides(fields: "username,email")
}

extend type User @key(fields: "id") {
  id: ID! @external
  username: String @external
  email: String @external
}
`;
