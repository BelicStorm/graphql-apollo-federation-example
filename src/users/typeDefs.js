import { gql } from "apollo-server-express";

export const typeDefs = gql`
extend type Query {
  me: [User]
}

type User @key(fields: "id") {
  id: ID!
  username: String,
  email: String,
  bio: String,
  hash: String,
  salt: String,
  idsocial: String,
}
`;
