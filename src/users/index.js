import { ApolloServer, gql } from "apollo-server-express";
import  { buildFederatedSchema } from '@apollo/federation';
import { ApolloGateway } from "@apollo/gateway";
import express from "express";
import mongoose from "mongoose";
import { resolvers } from "./resolvers";
import { typeDefs } from "./typeDefs";

const startServer = async () => {
  const app = express();

  const server = new ApolloServer({
    schema: buildFederatedSchema([
      {
        typeDefs,
        resolvers,
      },
    ]),
  });



  server.applyMiddleware({ app });

  await mongoose.connect("mongodb://localhost:27017/StarWarsTravel_test", {
    useNewUrlParser: true
  });

  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  );
};

startServer();
