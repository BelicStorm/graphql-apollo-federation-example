var mongoose = require('mongoose');
const Users_db = require('../models/Users');
let User = mongoose.model('User');

export const resolvers = {
  Query: {
    me() {
      return User.find()
    }
  },
  User: {
    __resolveReference(object) {

      return User.find(object.id);
    }
  }
 
};
