import { ApolloServer, gql } from "apollo-server-express";
import { ApolloGateway } from "@apollo/gateway";
import express from "express";

const startServer = async () => {
  const app = express();

  const gateway = new ApolloGateway({
    serviceList: [
      { name: 'Users', url: 'http://localhost:4000/graphql' },
      { name: 'Coments', url: 'http://localhost:4002/graphql' }
      // more services
    ],
  });
  
  // Pass the ApolloGateway to the ApolloServer constructor
  const server = new ApolloServer({
    gateway,
  
    // Disable subscriptions (not currently supported with ApolloGateway)
    subscriptions: false,
  });

  server.applyMiddleware({ app });

  app.listen({ port: 3999 }, () =>
    console.log(`🚀 Server ready at http://localhost:3999${server.graphqlPath}`)
    );
}
startServer()