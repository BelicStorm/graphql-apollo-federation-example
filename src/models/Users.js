var mongoose = require('mongoose');


var UserSchema = new mongoose.Schema({
    username: {type: String, lowercase: true, unique: true, index: true},
    email: {type: String, lowercase: true, unique: true, required: [true, "can't be blank"], match: [/\S+@\S+\.\S+/, 'is invalid'], index: true},
    bio: String,
    image: String,
    hash: String,
    salt: String,
    idsocial:String,
    favorites: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Planets' }]
    /* following: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }], */
  }, {timestamps: true});

  /* Authentication and creation users stuff */
  
  mongoose.model('User', UserSchema);